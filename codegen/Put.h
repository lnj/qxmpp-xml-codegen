// This file is auto-generated. All changes will vanish!

#pragma once

#include <QString>
#include <vector>

#include "Header.h"
#include "xmlserde.h"

namespace HttpUpload {

struct Put {
    QString url;
    std::vector<::HttpUpload::Header> headers;
};

}  // HttpUpload

template <>
struct XmlSerDe<HttpUpload::Put> {
    static void serialize(const HttpUpload::Put &d, QXmlStreamWriter &w);
    static DeserializeResult<HttpUpload::Put> deserialize(const QDomElement &input);
};
