// This file is auto-generated. All changes will vanish!

#pragma once

#include <QString>

#include "xmlserde.h"

namespace HttpUpload {

struct Retry {
    QString stamp;
};

}  // HttpUpload

template <>
struct XmlSerDe<HttpUpload::Retry> {
    static void serialize(const HttpUpload::Retry &d, QXmlStreamWriter &w);
    static DeserializeResult<HttpUpload::Retry> deserialize(const QDomElement &input);
};
