// This file is auto-generated. All changes will vanish!

#pragma once

#include <cstdint>

#include "xmlserde.h"

namespace HttpUpload {

struct MaxFileSize {
    uint64_t content;
};

}  // HttpUpload

template <>
struct XmlSerDe<HttpUpload::MaxFileSize> {
    static void serialize(const HttpUpload::MaxFileSize &d, QXmlStreamWriter &w);
    static DeserializeResult<HttpUpload::MaxFileSize> deserialize(const QDomElement &input);
};
