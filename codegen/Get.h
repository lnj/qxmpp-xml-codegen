// This file is auto-generated. All changes will vanish!

#pragma once

#include <QString>

#include "xmlserde.h"

namespace HttpUpload {

struct Get {
    QString url;
};

}  // HttpUpload

template <>
struct XmlSerDe<HttpUpload::Get> {
    static void serialize(const HttpUpload::Get &d, QXmlStreamWriter &w);
    static DeserializeResult<HttpUpload::Get> deserialize(const QDomElement &input);
};
