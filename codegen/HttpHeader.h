// This file is auto-generated. All changes will vanish!

#pragma once

namespace HttpUpload {

enum HttpHeader { Authorization, Cookie, Expires };

}  // HttpUpload

template <>
struct StringSerDe<HttpUpload::HttpHeader> {
    static QString serialize(HttpUpload::HttpHeader);
    static DeserializeResult<HttpUpload::HttpHeader> deserialize(QStringView);
};
