// This file is auto-generated. All changes will vanish!

#pragma once

#include "Get.h"
#include "Put.h"
#include "xmlserde.h"

namespace HttpUpload {

struct Slot {
    ::HttpUpload::Put put;
    ::HttpUpload::Get get;
};

}  // HttpUpload

template <>
struct XmlSerDe<HttpUpload::Slot> {
    static void serialize(const HttpUpload::Slot &d, QXmlStreamWriter &w);
    static DeserializeResult<HttpUpload::Slot> deserialize(const QDomElement &input);
};
