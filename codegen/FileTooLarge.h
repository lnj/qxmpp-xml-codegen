// This file is auto-generated. All changes will vanish!

#pragma once

#include <cstdint>
#include <optional>

#include "xmlserde.h"

namespace HttpUpload {

struct FileTooLarge {
    std::optional<uint64_t> maxFileSize;
};

}  // HttpUpload

template <>
struct XmlSerDe<HttpUpload::FileTooLarge> {
    static void serialize(const HttpUpload::FileTooLarge &d, QXmlStreamWriter &w);
    static DeserializeResult<HttpUpload::FileTooLarge> deserialize(const QDomElement &input);
};
