# SPDX-FileCopyrightText: 2022 Jonah Brüchert <jbb@kaidan.im>
#
# SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only

def parse_minus_case(identifier: str) -> list[str]:
    return [word.lower() for word in identifier.split("-")]


def parse_snake_case(identifier: str) -> list[str]:
    return [word.lower() for word in identifier.split("_")]


def to_camel_case(words: list[str]) -> str:
    out = words[0]
    for word in words[1:]:
        out += word[0].upper() + word[1:]
    return out


def to_upper_camel_case(words: list[str]) -> str:
    out = str()
    for word in words:
        out += word[0].upper() + word[1:]
    return out


def minus_to_camel_case(identifier: str) -> str:
    return to_camel_case(parse_minus_case(identifier))


def minus_to_upper_camel_case(identifier: str) -> str:
    return to_upper_camel_case(parse_minus_case(identifier))


def snake_to_camel_case(identifier: str) -> str:
    return to_camel_case(parse_snake_case(identifier))


def snake_to_upper_camel_case(identifier: str) -> str:
    return to_upper_camel_case(parse_snake_case(identifier))

