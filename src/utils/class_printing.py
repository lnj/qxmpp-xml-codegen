from json import JSONEncoder, dumps
from typing import Type


class JsonEncoder(JSONEncoder):
    def default(self, o):
        return o.__dict__


def dump(obj: any) -> str:
    return obj.__class__.__name__ + " " + dumps(obj.__dict__, indent=2, cls=JsonEncoder)


def printable(class_type: Type):
    # def mutate_instance():
    #     class_obj.__str__ = dump
    #     return class_obj
    class_type.__str__ = dump
    return class_type
