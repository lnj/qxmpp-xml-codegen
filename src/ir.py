# SPDX-FileCopyrightText: 2022 Jonah Brüchert <jbb@kaidan.im>
# SPDX-FileCopyrightText: 2022 Linus Jahn <lnj@kaidan.im>
#
# SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only

from enum import Enum, auto
from json import dumps, JSONEncoder
from typing import Optional

from utils.class_printing import printable


class ComponentNamespace:
    xmlns: str = ""
    namespace: str = ""


class Occurrence(Enum):
    Single = auto()
    Optional = auto()
    Vector = auto()

    def to_string(self) -> str:
        match self:
            case Occurrence.Single:
                return "Single"
            case Occurrence.Vector:
                return "Vector"
            case Occurrence.Optional:
                return "Optional"


@printable
class Attribute:
    xml_name: str
    name: str
    type: str
    full_type: str
    serde_type: str
    required: bool = False
    fallback_value: Optional[str]

    @property
    def type_occurrence(self):
        return Occurrence.Single if self.required else Occurrence.Optional

    @property
    def store_type(self):
        if self.required:
            return self.full_type
        return f"std::optional<{self.full_type}>"


@printable
class ElementContent:
    type: str
    serde_type: str
    name: str
    required: bool = True

    @property
    def type_occurrence(self):
        return Occurrence.Single if self.required else Occurrence.Optional

    @property
    def store_type(self):
        if self.required:
            return self.type
        return f"std::optional<{self.type}>"


@printable
class SubElement:
    type: str
    full_type: str
    serde_type: str
    name: str
    xml_name: str
    xmlns: str
    min_occurs: int
    max_occurs: int

    @property
    def type_occurrence(self) -> Occurrence:
        match [self.min_occurs, self.max_occurs]:
            case [1, 1]:
                return Occurrence.Single
            case [0, 1]:
                return Occurrence.Optional
        return Occurrence.Vector

    @property
    def store_type(self):
        match self.type_occurrence:
            case Occurrence.Single:
                return self.full_type
            case Occurrence.Optional:
                return f"std::optional<{self.full_type}>"
            case Occurrence.Vector:
                return f"std::vector<{self.full_type}>"
        raise ValueError(f"SubElement[type={self.type}] Invalid occurrence type set")


@printable
class Element:
    name: str
    xml_name: str
    xmlns: str
    namespace: str
    attributes: list[Attribute]
    sub_els: list[SubElement]
    content: Optional[ElementContent] = None

    def __init__(self):
        self.attributes = []
        self.sub_els = []

    @property
    def full_name(self):
        return f"{self.namespace}::{self.name}" if self.namespace else self.name


@printable
class EnumType:
    name: str
    xmlns: str
    namespace: str
    enumerators: list[str]
    xml_enumerators: list[str]

    def __init__(self):
        self.name = ""
        self.xmlns = ""
        self.xml_enumerators = []
        self.enumerators = []

    @property
    def full_name(self):
        return f"{self.namespace}::{self.name}" if self.namespace else self.name
