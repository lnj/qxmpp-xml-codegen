# SPDX-FileCopyrightText: 2024 Linus Jahn <lnj@kaidan.im>
#
# SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only
import dataclasses
import warnings

import ir
from utils.class_printing import printable
from utils.case_conversion import *


@printable
@dataclasses.dataclass
class XmlSpecElement:
    @printable
    @dataclasses.dataclass
    class Ref:
        name: str
        label: str | None
        default: str | None
        min: int | None
        max: int | None

        @staticmethod
        def from_json(j):
            assert set(j.keys()) <= {"name", "label", "default", "min", "max"}, ", ".join(j.keys())
            return XmlSpecElement.Ref(
                j["name"],
                j["label"] if "label" in j else None,
                j["default"] if "default" in j else None,
                int(j["min"]) if "min" in j else None,
                int(j["max"]) if "max" in j else None
            )

    @printable
    @dataclasses.dataclass
    class Attr:
        name: str
        label: str | None
        required: bool
        always_encode: bool
        default: str | None
        enc: list[str] | None
        dec: list[str] | None

        @staticmethod
        def from_json(j):
            assert set(j.keys()) <= {"name", "label", "required", "always_encode", "default", "enc", "dec"}, ", ".join(j.keys())
            return XmlSpecElement.Attr(
                name=j["name"],
                label=j["label"] if "label" in j else None,
                required="required" in j and j["required"],
                always_encode="always_encode" in j and j["always_encode"],
                default=j["default"] if "default" in j else None,
                enc=j["enc"] if "enc" in j else None,
                dec=j["dec"] if "dec" in j else None,
            )

    @printable
    @dataclasses.dataclass
    class CData:
        label: str | None
        required: bool
        default: str | None
        enc: list[str] | None
        dec: list[str] | None

        @staticmethod
        def from_json(j):
            assert set(j.keys()) <= {"label", "required", "default", "enc", "dec"}, ", ".join(j.keys())
            return XmlSpecElement.CData(
                j["label"] if "label" in j else None,
                j["required"] if "required" in j else False,
                j["default"] if "default" in j else None,
                j["enc"] if "enc" in j else None,
                j["dec"] if "dec" in j else None
            )


    record_name: str
    name: str
    xmlns: str
    module: str
    result: str | list[str]
    ignore_els: bool
    refs: list[Ref]
    attrs: list[Attr]
    cdata: CData | None

    @staticmethod
    def from_json(record_name, json):
        assert set(json.keys()) <= {"name", "xmlns", "module", "result", "ignore_els", "refs", "attrs", "cdata"}, \
                f"{record_name}: " + ", ".join(json.keys())
        return XmlSpecElement(
            record_name,
            json["name"],
            json["xmlns"],
            json["module"],
            json["result"],
            json["ignore_els"] if "ignore_els" in json else False,
            list(XmlSpecElement.Ref.from_json(value) for value in json["refs"]) if "refs" in json else [],
            list(XmlSpecElement.Attr.from_json(value) for value in json["attrs"]) if "attrs" in json else [],
            XmlSpecElement.CData.from_json(json["cdata"]) if "cdata" in json else None,
        )


def generate_ir(xml_spec_json: dict) -> tuple[list[ir.Element], list[ir.Enum], dict[str, ir.ComponentNamespace]]:
    spec_elements: list[XmlSpecElement] = [
        XmlSpecElement.from_json(el_name, element) for el_name, element in xml_spec_json.items()
    ]

    result_namespaces = dict()
    module_names = set(e.module for e in spec_elements)
    for name in module_names:
        xmlns_set = set()
        for e in spec_elements:
            if e.module == name:
                if isinstance(e.xmlns, list):
                    xmlns_set.update(set(e.xmlns))
                else:
                    xmlns_set.add(e.xmlns)

        for i, xmlns in enumerate(sorted(xmlns_set)):
            namespace = ir.ComponentNamespace()
            namespace.xmlns = xmlns
            namespace.namespace = snake_to_upper_camel_case(name)
            if len(xmlns_set) > 1:
                namespace.namespace += "_" + str(i + 1)
            result_namespaces[xmlns] = namespace

    result_elements = []
    for e in spec_elements:
        # print(snake_to_camel_case(el_name))
        if isinstance(e.result, list):
            print(e.record_name, "->", e.result)

            element: ir.Element = ir.Element()
            element.name = snake_to_upper_camel_case(e.record_name)
            element.xml_name = e.name
            if isinstance(e.xmlns, list):
                warnings.warn(f"{e.record_name}: Multiple xmlns not supported, using last")
                element.xmlns = e.xmlns[-1]
            else:
                element.xmlns = e.xmlns
            element.namespace = to_upper_camel_case([e.module])
            for a in e.attrs:
                attribute: ir.Attribute = ir.Attribute()
                if a.label is not None:
                    assert a.label[0] == "$", a.label
                    attribute.name = snake_to_camel_case(a.label[1:])
                else:
                    attribute.name = minus_to_camel_case(a.name)
                attribute.xml_name = a.name
                attribute.type = "QString"
                if a.dec:
                    warnings.warn(f"{e.record_name}/{a.label}: Custom type {str(a.dec)} not supported")
                attribute.full_type = "QString"
                attribute.serde_type = f"StringSerDe<{attribute.type}>"
                attribute.required = a.required or a.default

                element.attributes.append(attribute)

            if e.cdata is not None:
                element.content = ir.ElementContent()
                if e.cdata.label is not None:
                    assert e.cdata.label[0] == "$", e.cdata.label
                    element.content.name = snake_to_camel_case(e.cdata.label[1:])
                else:
                    warnings.warn(f"{e.record_name}/cdata: No label given")
                    element.content.name = "content"

                if e.cdata.dec:
                    warnings.warn(f"{e.record_name}/cdata: Custom type {str(e.cdata.dec)} not supported")
                element.content.type = "QString"
                element.content.serde_type = "StringSerDe<QString>"
                element.content.required = e.cdata.required

            for ref in e.refs:
                # look up element by name in spec_elements
                referenced_element: XmlSpecElement | None = None
                for other_element in spec_elements:
                    if other_element.record_name == ref.name:
                        referenced_element = other_element

                if referenced_element is None:
                    warnings.warn(f"{e.record_name}/ref/{ref.name}: Could not find referenced sub-element type!")
                    assert False

                sub_el: ir.SubElement = ir.SubElement()
                if ref.label is not None:
                    sub_el.name = snake_to_camel_case(ref.label[1:])
                else:
                    sub_el.name = snake_to_camel_case(ref.name)
                sub_el.xml_name = referenced_element.name
                sub_el.xmlns = referenced_element.xmlns
                sub_el.type = snake_to_upper_camel_case(referenced_element.record_name)
                sub_el.full_type = f"::{snake_to_upper_camel_case(referenced_element.module)}::{sub_el.type}"
                sub_el.serde_type = f"XmlSerDe<{sub_el.full_type}>"
                sub_el.min_occurs = ref.min if ref.min is not None else 0
                sub_el.max_occurs = ref.max if ref.max is not None else -1

                element.sub_els.append(sub_el)

            result_elements.append(element)

        else:
            # TODO: support $_els
            # print(e.record_name, "->", e.result)
            pass

    return result_elements, [], result_namespaces
