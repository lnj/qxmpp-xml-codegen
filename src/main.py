#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2022 Linus Jahn <lnj@kaidan.im>
# SPDX-FileCopyrightText: 2022 Jonah Brüchert <jbb@kaidan.im>
#
# SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only

import sys
import json
from pathlib import Path
from xml.etree import ElementTree

import irgen
import xml_spec_irgen
import xsd
import codegen


def generate_qt_classes(state: irgen.State):
    print("\nIR:")
    for e in state.elements:
        print(e)

    print("Generating code…")
    codegen.generate_code(state.elements, state.enums, state.namespaces)


def generate_from_xsd(schema_dir: str):
    schema_path = Path(schema_dir)
    components: list[xsd.Component] = []
    for file in schema_path.iterdir():
        if not file.is_file():
            continue

        tree = ElementTree.parse(file)
        component = xsd.Component.from_xml(tree)
        if component:
            components.append(component)

    state: irgen.State = irgen.generate_ir(components)

    generate_qt_classes(state)


def generate_from_xml_spec(xml_spec_json_file: str):
    state = irgen.State()
    state.elements, state.enums, state.namespaces = xml_spec_irgen.generate_ir(
        json.load(open(xml_spec_json_file))
    )
    generate_qt_classes(state)


if __name__ == '__main__':
    try:
        _, input_type, path = sys.argv
    except:
        print(f"Usage: {sys.argv[0]} [xsd|xml-spec] [xsd include path|xml-spec json file]")
        exit(1)

    match input_type:
        case "xsd":
            generate_from_xsd(path)
        case "xml-spec":
            generate_from_xml_spec(path)
        case _:
            print("Invalid argument: Must be xsd or xml-spec")
            exit(1)
