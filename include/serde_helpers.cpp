// SPDX-FileCopyrightText: 2022 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only

#include "serde_helpers.h"

#include <QDebug>
#include <QBitArray>
#include <ranges>
#include <bitset>

namespace XmlUtils {

QDomElement first_child(const QDomElement &el, QStringView tagName, QStringView xmlns)
{
    for (auto node = el.firstChild(); !node.isNull(); node = node.nextSibling()) {
        if (node.isElement() && node.namespaceURI() == xmlns) {
            auto sub_el = node.toElement();
            if (sub_el.tagName() == tagName) {
                return sub_el;
            }
        }
    }
    return {};
}

QDomElement next_sibling(const QDomElement &el, QStringView tagName, QStringView xmlns)
{
    for (auto node = el.nextSibling(); !node.isNull(); node = node.nextSibling()) {
        if (node.isElement() && node.namespaceURI() == xmlns) {
            auto sub_el = node.toElement();
            if (sub_el.tagName() == tagName) {
                return sub_el;
            }
        }
    }
    return {};
}

QStringList missing_attributes(const QDomElement &el, std::initializer_list<QStringView> attributes)
{
    Q_ASSERT(attributes.size() <= 32);
    std::bitset<32> found_attributes(0);
    auto actual_attributes = el.attributes();
    for (int i = actual_attributes.size(); i >= 0; i--) {
        auto itr = std::ranges::find(attributes, actual_attributes.item(i).toAttr().name());
        if (itr != attributes.end()) {
            found_attributes.set(size_t(itr - attributes.begin()), true);
        }
    }
    std::bitset<32> expected(0);
    for (size_t i = 0; i < attributes.size(); i++) {
        expected.set(i, true);
    }
    if (found_attributes == expected) {
        return {};
    }

    QStringList missing_attribute_names;
    for (auto itr = attributes.begin(); itr != attributes.end(); ++itr) {
        auto i = attributes.end() - itr;
        if (not found_attributes.test(i)) {
            missing_attribute_names << itr->toString();
        }
    }
    return missing_attribute_names;
}

std::optional<XmlDeserializeError> check_missing_attributes(const QDomElement &el, std::initializer_list<QStringView> attributes)
{
    auto missing = missing_attributes(el, std::move(attributes));
    if (!missing.isEmpty()) {
        return XmlDeserializeError {
            u"Missing required attributes: " % missing.join(u", "),
            XmlDeserializeError::MissingAttribute
        };
    }
    return {};
}

}
