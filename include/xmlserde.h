#pragma once

#include <variant>

#include <QDomElement>
#include <QXmlReader>
#include <QXmlStreamWriter>

struct XmlDeserializeError {
    QString message;
    enum {
        ElementsCountMismatch,
        MissingAttribute,
        MissingContent,
        InvalidEnumValue,
        InvalidValue,
    } type;
};

template<typename SuccessType>
using DeserializeResult = std::variant<SuccessType, XmlDeserializeError>;

template <typename T>
struct XmlSerDe;
template <typename T>
struct StringSerDe;
