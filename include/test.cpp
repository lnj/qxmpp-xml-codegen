#include "../codegen/Slot.h"
#include <QDomDocument>
#include <QDebug>

using namespace HttpUpload;

template <typename T>
auto serialize(T value)
{
    QString output;
    QXmlStreamWriter writer(&output);
    XmlSerDe<T>::serialize(value, writer);
    return output;
}

int main()
{
    auto slotString = QStringLiteral(R"(<slot xmlns="urn:xmpp:http:upload:0"><put xmlns="urn:xmpp:http:upload:0" url="https://upload.montague.tld/4a771ac1-f0b2-4a4a-9700-f2a26fa2bb67/tr%C3%A8s%20cool.jpg"><header xmlns="urn:xmpp:http:upload:0" name="Authorization">Basic Base64String==</header><header xmlns="urn:xmpp:http:upload:0" name="Cookie">foo=bar; user=romeo</header></put><get xmlns="urn:xmpp:http:upload:0" url="https://download.montague.tld/4a771ac1-f0b2-4a4a-9700-f2a26fa2bb67/tr%C3%A8s%20cool.jpg"/></slot>)");
    QDomDocument doc;
    doc.setContent(slotString, true);
    auto slotDom = doc.documentElement();

    auto result = XmlSerDe<Slot>::deserialize(slotDom);
    if (auto err = std::get_if<XmlDeserializeError>(&result)) {
        qDebug() << err->message << err->type;
        return 1;
    }

    auto slot = std::get<Slot>(std::move(result));
    qDebug() << "Get url:" << slot.get.url;
    qDebug() << "Put url:" << slot.put.url;
    qDebug() << "Put headers:";
    for (const auto &header : slot.put.headers) {
        qDebug() << " *" << header.name << header.value;
    }

    auto output = serialize(slot);
    qDebug() << output;
    qDebug() << "Match:" << (output == slotString);

    auto slot2 = Slot {
        Put {
            "https://upload.montague.tld/4a771ac1-f0b2-4a4a-9700-f2a26fa2bb67/tr%C3%A8s%20cool.jpg",
            {
                Header { Authorization, "Basic Base64String==" },
                Header { Cookie, "foo=bar; user=romeo" },
            }
        },
        Get {
            "https://download.montague.tld/4a771ac1-f0b2-4a4a-9700-f2a26fa2bb67/tr%C3%A8s%20cool.jpg"
        }
    };

    qDebug() << "serialized from new" << serialize(slot2);
    qDebug() << "matches:" << (serialize(slot2) == slotString);

    return 0;
}
