// SPDX-FileCopyrightText: 2022 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only

#pragma once

#include <variant>
#include <optional>
#include <vector>
#include <limits>

#include <QDomElement>
#include <QStringBuilder>
#include <QXmlStreamWriter>

#include "xmlserde.h"

namespace XmlUtils {

QDomElement first_child(const QDomElement &el, QStringView tagName, QStringView xmlns);
QDomElement next_sibling(const QDomElement &el, QStringView tagName, QStringView xmlns);

QStringList missing_attributes(const QDomElement &el, std::initializer_list<QStringView>);
std::optional<XmlDeserializeError> check_missing_attributes(const QDomElement &el, std::initializer_list<QStringView>);

    enum Occurrence {
        Single,
        Optional,
        Vector,
    };

    template <Occurrence occ, typename T>
    auto occurrence_type_helper()
    {
        if constexpr (occ == Single) {
            return T {};
        } else if constexpr (occ == Optional) {
            return std::optional<T> {};
        } else if constexpr (occ == Vector) {
            return std::vector<T> {};
        }
    }

    template <typename T>
    using DeserializeResult = std::variant<T, XmlDeserializeError>;
    
    template <Occurrence occ, typename T>
    using OccurrenceType = decltype(occurrence_type_helper<occ, T>());

template<size_t N>
struct StringLiteral {
    constexpr StringLiteral(const char16_t (&str)[N]) {
        std::copy_n(str, N, value);
    }

    QStringView view() const {
        return QStringView(value);
    }
    QString toString() const {
        // could be optimized by using something like QStringLiteral()
        return view().toString();
    }

    char16_t value[N];
};

template <typename T>
auto map_to_optional(DeserializeResult<T> &&result) -> DeserializeResult<std::optional<T>>
{
    if (std::holds_alternative<T>(result)) {
        return std::optional { std::get<T>(std::move(result)) };
    }
    return std::get<XmlDeserializeError>(std::move(result));
}

    template <Occurrence occ, typename Type, typename SerDe>
    auto serialize(const OccurrenceType<occ, Type> &d, QXmlStreamWriter &w) {
        if constexpr (occ == Single) {
            SerDe::serialize(d, w);
        } else if constexpr (occ == Optional) {
            if (d) {
                SerDe::serialize(*d, w);
            }
        } else if constexpr (occ == Vector) {
            for (const auto &item : d) {
                SerDe::serialize(item, w);
            }
        }
    }

    template <Occurrence occ, typename Type, typename SerDe>
    void serialize_attribute(const OccurrenceType<occ, Type> &d, const QString &attributeName, QXmlStreamWriter &w) {
        if constexpr (occ == Single) {
            w.writeAttribute(attributeName, SerDe::serialize(d));
        } else if constexpr (occ == Optional) {
            if (d) {
                w.writeAttribute(attributeName, SerDe::serialize(*d));
            }
        }
    }

    template <Occurrence occ, typename Type, typename SerDe>
    void serialize_content(const OccurrenceType<occ, Type> &d, QXmlStreamWriter &w) {
        if constexpr (occ == Single) {
            w.writeCharacters(SerDe::serialize(d));
        } else if constexpr (occ == Optional) {
            if (d) {
                w.writeCharacters(SerDe::serialize(*d));
            }
        }
    }

    template <int minCount, int maxCount>
    std::optional<XmlDeserializeError> check_elements_count(int actualCount) {
        if constexpr (maxCount == -1) {
            if (actualCount >= minCount) {
                return {};
            }
            return XmlDeserializeError {
                u"Too few subelements, must be at least " % QString::number(minCount) % u", found " %
                QString::number(actualCount) % ".",
                XmlDeserializeError::ElementsCountMismatch
            };
        } else {
            if (actualCount >= minCount && actualCount <= maxCount) {
                return {};
            }

            return XmlDeserializeError {
                u"Subelement count mismatch, must be between " % QString::number(minCount) % u" and " %
                QString::number(maxCount) % u", found " % QString::number(actualCount) % ".",
                XmlDeserializeError::ElementsCountMismatch
            };
        }
    }

    template <Occurrence occurrence, typename Type, typename SerDe, int minCount, int maxCount>
    auto deserialize(const QDomElement &element, QStringView childName, QStringView childXmlNs)
            -> DeserializeResult<OccurrenceType<occurrence, Type>> {
        if constexpr (occurrence == Optional) {
            auto el = first_child(element, childName, childXmlNs);
            if (el.isNull()) {
                return std::optional<Type>();
            } else {
                if (!next_sibling(el, childName, childXmlNs).isNull()) {
                    return XmlDeserializeError {
                        QStringLiteral("More than one element for optional type!"),
                        XmlDeserializeError::ElementsCountMismatch
                    };
                }
                return map_to_optional(SerDe::deserialize(el));
            }
        } else if constexpr (occurrence == Single) {
            auto el = first_child(element, childName, childXmlNs);
            if (el.isNull()) {
                return XmlDeserializeError {
                    u"Missing required element \"" % childName % "\".",
                    XmlDeserializeError::ElementsCountMismatch
                };
            }
            if (!next_sibling(el, childName, childXmlNs).isNull()) {
                return XmlDeserializeError {
                    u"Too many \"" % childName % "\" elements found, can only handle one.",
                    XmlDeserializeError::ElementsCountMismatch
                };
            }
            return SerDe::deserialize(el);
        } else if constexpr (occurrence == Vector) {
            std::vector<Type> out;
            for (auto el = first_child(element, childName, childXmlNs);
                 !el.isNull();
                 el = next_sibling(el, childName, childXmlNs)) {
                auto result = SerDe::deserialize(el);
                if (std::holds_alternative<XmlDeserializeError>(result)) {
                    return std::get<XmlDeserializeError>(std::move(result));
                }
                out.push_back(std::get<Type>(std::move(result)));
            }
            if (auto err = check_elements_count<minCount, maxCount>(out.size())) {
                return std::move(*err);
            }
            return out;
        }
    }

template <typename StringSerDe, StringLiteral tagName>
struct ElementContentSerDe {
    using ContentType = std::decay_t<decltype(std::get<0>(StringSerDe::deserialize({})))>;

    static void serialize(const ContentType &content, QXmlStreamWriter &w) {
        w.writeTextElement(tagName.toString(), StringSerDe::serialize(content));
    }
    static DeserializeResult<ContentType> deserialize(const QDomElement &el) {
        auto result = StringSerDe::deserialize(el.text());
        if (std::holds_alternative<XmlDeserializeError>(result)) {
            return std::get<XmlDeserializeError>(std::move(result));
        }
        return std::get<ContentType>(std::move(result));
    }
};

}

template <>
struct StringSerDe<QString> {
    static const QString &serialize(const QString &in) {
        return in;
    }
    static DeserializeResult<QString> deserialize(const QString &d) {
        return d;
    }
};

template <typename Int>
struct IntegerSerDe
{
    static Int string_to_int(const QString &str, bool *ok) {
        if constexpr (std::is_same_v<Int, int8_t>) {
            auto result = str.toShort(ok);
            if (ok && result <= std::numeric_limits<int8_t>().max() && result >= std::numeric_limits<int8_t>().min()) {
                return int8_t(result);
            }
            *ok = false;
            return 0;
        } else if constexpr (std::is_same_v<Int, uint8_t>) {
            auto result = str.toUShort(ok);
            if (ok && result <= std::numeric_limits<int8_t>().max() && result >= std::numeric_limits<int8_t>().min()) {
                return int8_t(result);
            }
            *ok = false;
            return 0;
        } else if constexpr (std::is_same_v<Int, int16_t>) {
            return str.toShort(ok);
        } else if constexpr (std::is_same_v<Int, uint16_t>) {
            return str.toUShort(ok);
        } else if constexpr (std::is_same_v<Int, int32_t>) {
            return str.toInt(ok);
        } else if constexpr (std::is_same_v<Int, uint32_t>) {
            return str.toUInt(ok);
        } else if constexpr (std::is_same_v<Int, int64_t>) {
            return str.toLongLong(ok);
        } else if constexpr (std::is_same_v<Int, uint64_t>) {
            return str.toULongLong(ok);
        }
    }

    static QString serialize(Int val) {
        return QString::number(val);
    }
    static DeserializeResult<Int> deserialize(const QString &str) {
        bool ok;
        auto result = string_to_int(str, &ok);
        if (!ok) {
            return XmlDeserializeError {
                u"Invalid integer value \"" % str % u"\".",
                XmlDeserializeError::InvalidValue
            };
        }
        return result;
    }
};

struct DoubleSerDe {
    static QString serialize(double val) {
        return QString::number(val);
    }
    static DeserializeResult<double> deserialize(const QString &str) {
        bool ok;
        auto result = str.toDouble(&ok);
        if (!ok) {
            return XmlDeserializeError {
                u"Invalid float value \"" % str % u"\".",
                XmlDeserializeError::InvalidValue
            };
        }
        return result;
    }
};
