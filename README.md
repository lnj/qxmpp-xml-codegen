# QXmpp XML Schema Codegen

## TODO

- [ ] Add namespace check on parsing
- [x] Return errors when parsing subels
- [ ] Cross namespace references
- [x] Code generation of C++ namespaces
- [ ] Implement more basic types:
  - [ ] JID
  - [ ] date/Time/dateTime
  - [x] decimal/integer types (missing some more checks e.g. for "positiveInteger")
  - [ ] boolean
  - [ ] base64Binary
  - [ ] hexBinary
  - [ ] anyURI
  - [ ] NMTOKEN
  - [ ] and many more?
- [ ] Implement more restrictions (length, min/max length, min/max (digit count))
- [x] Move SerDe implementation into cpp
- [ ] off topic: Create a JID class

Open questions:
- Handle when to write xmlns and when not
- Same elements in multiple namespaces (e.g. pubsub, pubsub#owner or jabber:client, jabber:server, jabber:component)
  - Generate same code multiple times in different C++-namespaces?
  - Add xmlns attribute (as string or enum?)
  - 
